﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
//using UnityEngine.SceneManagement;

public class PlayerStamina : MonoBehaviour 
{
	public float maxStamina = 100;
	public Slider staminaSlider;
	public bool moveUsed = false;
	public float staminaRegen;
	public Image Fill;

	void Awake()
	{
		staminaSlider.value = maxStamina;
		staminaRegen = 10;
	}
	
	// Update is called once per frame
	void Update () {
		if (moveUsed)
			staminaRegen = 0;
		else
			staminaRegen = 20;
		if (staminaSlider.value <= 0) {
			Fill.color = Color.clear;
		}
		if (!moveUsed) {
			if (staminaSlider.value != maxStamina) {
				Fill.color = new Color (26f / 255f, 77f / 255f, 24f / 255f, 1f);
				staminaSlider.value += staminaRegen * Time.deltaTime;
				if (staminaSlider.value > maxStamina) {
					staminaSlider.value = maxStamina;
				}
			}
		}
	}

	public void addStamina(float addedStamina) {
		maxStamina += addedStamina;
		staminaSlider.maxValue = maxStamina;
	}
}
