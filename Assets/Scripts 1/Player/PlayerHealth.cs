﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
//using UnityEngine.SceneManagement;

public class PlayerHealth : MonoBehaviour {
	public Slider healthBar;
	public float maxHealth = 100;
	public Image Fill;

	private Animator anim;

	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator> ();
		healthBar.value = maxHealth;
	}
	// Update is called once per frame
	void Update () {

		if (healthBar.value <= 0) {
			Fill.color = Color.clear;
			Debug.Log ("You died.");
			//SceneManager.LoadScene (2);
		} 
	}

	public void takeDamage(float damage) {

		if(!PlayerController.dodge) {
			healthBar.value -= damage;
			anim.SetTrigger ("Stagger");
			Debug.Log (healthBar.value);
		}
	}

	public void takeHpPack(float hpPack)
	{
		healthBar.value += hpPack;
	}
	
	public void addHealth(float addedHealth) {
		maxHealth += addedHealth;
		healthBar.maxValue = maxHealth;
	}
}
