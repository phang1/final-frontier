﻿using UnityEngine;
using System.Collections;

public class health : MonoBehaviour {
	public float hp = 500;
	public bool mark = false;

	private Animator anim;

	void Start() {
		anim = GetComponent<Animator> ();
	}

	// Update is called once per frame
	void Update () {
		if (hp <= 0) {
			Destroy (this.gameObject);
		}
	}

	public void Damage(int damage) {
		hp -= damage;
		anim.SetTrigger ("Stagger");
		Debug.Log (hp);
	}
}
