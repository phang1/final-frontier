﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour
{
    public static GameObject target;
    public Transform lookAt;
    public float hrotateSpeed = 2f;
    public float vrotateSpeed = 1f;
    public float zoomSpeed = 2f;
    public float camSpeed = 4f;

    GameObject[] enemies;
	int currentEnemy;

    Vector3 initialOffset;
    Vector3 currentOffset;
    Vector3 myPosition;
    float hAngle, vAngle, zoom;

    bool locked = false;
    float interp = 0f;

    // Use this for initialization
    void Start()
    {
        initialOffset = lookAt.transform.position - transform.position;
        hAngle = lookAt.transform.eulerAngles.y;
        vAngle = lookAt.transform.eulerAngles.x;
        zoom = .5f;
        //Don't show cursor/let it out of the screen
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame

    void Update()
    {
        //---------------------------------------------------------------------locking on----------------------------
        if (Input.GetMouseButtonDown(2))
        {
            if (locked)
            {
                target = null;
                locked = false;
            }
            else
            {
                enemies = GameObject.FindGameObjectsWithTag("Enemy");
				currentEnemy = 0;
				target = enemies[currentEnemy];
                locked = true;
                interp = 1.0f;
            }
        }

		if (locked) {
			float lockOnChange = Input.GetAxisRaw ("Mouse ScrollWheel");
			if (lockOnChange != 0) {

				if (lockOnChange < 0)
					currentEnemy--;
				else if (lockOnChange > 0)
					currentEnemy++;

				if (currentEnemy > enemies.Length) {
					currentEnemy = 0;
				}
				if (currentEnemy < 0) {
					currentEnemy = enemies.Length;
				}

				target = enemies [currentEnemy];
			}
		}
    }

    void LateUpdate()
    {
        if (!locked)
        {
            //----------------------------------------------------------camera rotation--------------------------------------------------------------
            //calculate new angle for offset based on input + old angle
            hAngle = hAngle + Input.GetAxis("Mouse X") * hrotateSpeed;
            vAngle = vAngle - Input.GetAxis("Mouse Y") * vrotateSpeed;

            //calculate new zoom
            zoom = zoom - Input.GetAxis("Mouse ScrollWheel") * zoomSpeed;

            //limit zoom/vertical angle to certain bounds
            zoom = Mathf.Clamp(zoom, .2f, 1.40f);
            vAngle = Mathf.Clamp(vAngle, -15, 60);
            //rotate offset horizontaly by hAngle and vertically by vAngle to be added to player's position
            Quaternion Rotation = Quaternion.Euler(vAngle, hAngle, 0);
            Vector3 endingPos = lookAt.transform.position - (Rotation * initialOffset * zoom);
            //interp forces the camera to interpolate between the locked position and the unlocked position
            if (interp > 0)
            {
                transform.position = Vector3.Lerp(transform.position, endingPos, 1 - interp);
                interp -= Time.deltaTime * camSpeed;
            }
            else
            {
                transform.position = endingPos;
            }
            transform.LookAt(lookAt.transform); 
        }
        else
        {
            Vector3 direction = target.transform.position - lookAt.position;
			direction.y = lookAt.position.y;
            currentOffset = -direction.normalized * 20;
			Debug.Log (currentOffset);
            this.transform.position = Vector3.Lerp(this.transform.position, lookAt.position + currentOffset, Time.deltaTime * camSpeed);
            //transform.position = lookAt.position + currentOffset;
            this.transform.LookAt(lookAt);
        }
    }
    
    bool CompareVectors(Vector3 a, Vector3 b)
    {
        return Vector3.SqrMagnitude(a - b) < 0.0001;
    }
}
