using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerController : MonoBehaviour {
	public static float speed = 20f;
	private float time;
	public float multiplier = 1.25f;
//	public GameObject DaggerCurve;
	public Animator anim;
//	public CharacterController player;
	public Camera mainCamera;
	public static bool dodge = false;
	public static bool lifeSteal;
	public Rigidbody player;

	private bool stagger = false;
	private float h;
	private float v;
	private PlayerStamina stam;
	private PlayerHealth health;

	public Actives actives;

	// Use this for initialization
	void Start () {
        // Set up references..
        anim = GetComponent <Animator> ();
		stam = GetComponent<PlayerStamina> ();
		health = GetComponent<PlayerHealth>();
		actives = GetComponent<Actives> ();
	}
	
	void Update() {
		anim.ResetTrigger ("big");
		anim.speed = 1f * multiplier;
		h = Input.GetAxis("Horizontal");
		v = Input.GetAxis("Vertical");

		if (v != 0 || h != 0) {
			Vector3 newForward = mainCamera.transform.forward;
			newForward.y = 0;

			Vector3 movement = Vector3.Normalize (v * newForward + h * mainCamera.transform.right) * speed;
			player.velocity = movement;
//			player.SimpleMove (movement);
			
			this.transform.rotation = Quaternion.Slerp (this.transform.rotation, Quaternion.LookRotation (movement + .0001f * newForward), Time.deltaTime * 10f);

			anim.SetFloat ("speed", movement.magnitude * speed);
		} else if (!dodge) {
			anim.SetFloat ("speed", 0);
			player.velocity = Vector3.zero;
		}

		if (Input.GetKeyDown (KeyCode.Space) && h == 0 && v == 0 && stam.staminaSlider.value > 0) {
			anim.SetTrigger ("Dodge");
		} else if (Input.GetKeyDown (KeyCode.Space) && stam.staminaSlider.value > 0) {
			anim.SetTrigger ("Dodge");
		}

		if(Input.GetKeyDown (KeyCode.Q)) {
			multiplier = 2.0f;
			speed = 30f;
			lifeSteal = true;
			Invoke("EndLifesteal", 10f);
		}
			

		if(Input.GetMouseButtonDown(0) && stam.staminaSlider.value > 0) {
			anim.SetTrigger("strike");
		}
		if (Input.GetMouseButtonDown (1)&& stam.staminaSlider.value > 0) {
			anim.SetTrigger("big");
		}
		/*if (Input.GetKeyDown (KeyCode.E) && PowerUps.isMarked) {
			PowerUps.isMarked = false;
			transform.position = new Vector3(PowerUps.target.transform.position.x, PowerUps.target.transform.position.y, PowerUps.target.transform.position.z + PowerUps.target.transform.forward.z * -5);
		} else if (Input.GetKeyDown (KeyCode.E)) {
			GameObject temp = (GameObject)Instantiate (DaggerCurve, new Vector3 (transform.position.x, transform.position.y + 1f, transform.position.z), transform.rotation);
			
			temp.GetComponent<Rigidbody> ().AddForce (400 * transform.forward.x, transform.position.y, 400f * transform.forward.z);
		} */

		if (Input.GetKeyDown (KeyCode.E)) {
				SoulMass.used = true;
			actives.cooldown ();
		}
		if (Input.GetKeyDown (KeyCode.R)) {
			if (actives.activeOrNot [6]) {

				actives.cooldown2 ();
			}
			if (actives.activeOrNot [7]) {

				actives.cooldown2 ();
			}
			if (actives.activeOrNot [8]) {
				SoulMass.used = true;
				actives.cooldown2 ();
			}
			if (actives.activeOrNot [9]) {

				actives.cooldown2 ();
			}
			if (actives.activeOrNot [10]) {

				actives.cooldown2 ();
			}
			if (actives.activeOrNot [11]) {

				actives.cooldown2 ();
			}
		}

		if (Input.GetKeyDown (KeyCode.E)) {
			SoulMass.used = true;
		}
//		if (dodge) {
//			if(h == 0 && v == 0) {
//				player.SimpleMove (-transform.forward * 4000f * Time.deltaTime);
//			}
//			else
//				player.SimpleMove (transform.forward * 4000f * Time.deltaTime);
//		}
//		if (stagger) {
//			player.SimpleMove(-transform.forward * 500f * Time.deltaTime);
//		}
//		if (PlayerWeaponCollider.attacking && h == 0 && v == 0) {
//			player.SimpleMove(transform.forward * 200f * Time.deltaTime);
//		}
	}

	public void Dodge() {
		dodge = true;
		stam.staminaSlider.value -= 10f;
		stam.moveUsed = true;
		player.velocity = player.transform.forward * speed;
		Invoke("EndDodge", .3f);
	}
	
	public void Stagger() {
		stagger = true;
		Invoke("EndStagger", .2f);
	}
	
	public void EndDodge() {
		dodge = false;
		player.velocity = Vector3.zero;
		stam.moveUsed = false;
	}

	public void EndStagger() {
		stagger = false;
	}

	public void EndLifesteal() {
		multiplier = 1.25f;
		speed = 10f;
		lifeSteal = false;
		CancelInvoke();
	}
}
