﻿using UnityEngine;
using System.Collections;

public class activateEnemies : MonoBehaviour {

	public EnemySight enemyOne;
	public EnemySight enemyTwo;
	public EnemySight enemyThree;
	public EnemySight enemyFour;

	// Use this for initialization
	void Start () {
		if(enemyOne != null)
			enemyOne.enabled = false;
		if(enemyTwo != null)
			enemyTwo.enabled = false;
		if(enemyThree != null)
			enemyThree.enabled = false;
		if(enemyFour != null)
			enemyFour.enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider other){
		if (other.CompareTag("Player")) {
			if(enemyOne != null)
				enemyOne.enabled = true;
			if(enemyTwo != null)
				enemyTwo.enabled = true;
			if(enemyThree != null)
				enemyThree.enabled = true;
			if(enemyFour != null)
				enemyFour.enabled = true;
		}
	}
}
