﻿using UnityEngine;
using System.Collections;

public class PowerUps : MonoBehaviour {
	public static Collider target;
	public static bool isMarked;

	void OnTriggerEnter(Collider other) {
		if (other.gameObject.CompareTag("enemy")) {
			Destroy (this.gameObject);
			health mark = other.GetComponent<health> ();
			mark.mark = true;
			isMarked = mark.mark;
			target = other;
			Debug.Log (isMarked);
		}
	}
}
