﻿using UnityEngine;
using System.Collections;

public class SoulMass : MonoBehaviour {

	public static bool used = false;
	public GameObject prefab;

	private float delay = 8f;
	private float timestamp;
	private GameObject player; 
	private Vector3 location;

	void Start() {
		player = GameObject.FindGameObjectWithTag ("Player");
	}

	// Update is called once per frame
	void Update () {
		if (used) {
			
			Invoke ("Spawn", .5f);
			used = false;
		}
	}

	public void Spawn() {
		location.x = player.transform.position.x;
		location.y = player.transform.position.y + 10f;
		location.z = player.transform.position.z;
		Instantiate (prefab, location, Quaternion.identity);
	}
}
