﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Seek : MonoBehaviour {

	public int damage = 20;

	private GameObject enemy;
	private EnemyHealth health;
	private bool seek = false;
	private GameObject camera;
	private Vector3 location;
	// Use this for initialization
	void Start () {
		camera = GameObject.FindGameObjectWithTag("MainCamera");
		enemy = GameObject.FindGameObjectWithTag ("Enemy");
	}
	
	// Update is called once per frame
	void Update () { 
		try {
			Invoke("Attack", 2f);
			if(seek) {
				Vector3 aim = enemy.transform.position + transform.up * 10f;
				transform.position = Vector3.MoveTowards (transform.position, aim, 1f);
				Invoke ("Remove", 5f);
			}
		}
		catch(UnityException e) {
			Debug.Log (e.Message);
		}
	}

	public void Attack() {
		seek = true;
	}

	public void Remove() {
		Destroy (gameObject);
	}

	void OnTriggerStay(Collider other) {
		if (other.CompareTag ("Enemy")) {
			health = other.GetComponent<EnemyHealth> ();
			health.TakeDamage (damage);
			Destroy (gameObject);
		}
	}
}
