﻿using UnityEngine;
using System.Collections;

public class RecoverHealth : MonoBehaviour {

	public int health;

	private PlayerHealth playerHealth;

	// Use this for initialization
	void Start () {
		playerHealth = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerHealth>();
	}

	void Update() {
		transform.Rotate(Vector3.forward + Vector3.right * 50f * Time.deltaTime);
	}
	
	public void healHealth() {
		health = 10;
		playerHealth.takeHpPack(health);
	}

	public void OnTriggerEnter(Collider other) {
		if(other.CompareTag("Player")) {
			healHealth();
			Destroy(gameObject);
		}
	}
}
