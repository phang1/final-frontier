﻿using UnityEngine;
using System.Collections;

public class AddStamina : MonoBehaviour {

	public int stamina;

	private PlayerStamina playerStam;

	// Use this for initialization
	void Start () {
		playerStam = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerStamina>();
	}
	
	void Update() {
		transform.Rotate(Vector3.forward * 50f * Time.deltaTime);
	}
	
	public void generateStamina() {
		stamina = Random.Range(3, 7);
		playerStam.addStamina(stamina);
	}

	public void OnTriggerEnter(Collider other) {
		if(other.CompareTag("Player")) {
			generateStamina();
			GameObject parent = this.transform.parent.gameObject;
			Destroy(parent);
		}
	}
}