﻿using UnityEngine;
using System.Collections;

public class AddDamage : MonoBehaviour {

	public int damage;

	private GameObject[] weapons;
	private Weapon[] weapon;

	// Use this for initialization
	void Start () {
		weapons = GameObject.FindGameObjectsWithTag("PlayerWeapon");
		weapon = new Weapon[2]; 
	}

	void Update() {
		transform.Rotate(Vector3.forward * 50f * Time.deltaTime);
	}
	
	public void generateDamage() {
		damage = Random.Range(2, 5);
		for(int i = 0; i < weapon.Length; i++)
			weapon[i].damage += damage;
	}

	public void OnTriggerEnter(Collider other) {
		if(other.CompareTag("Player")) {
			for(int i = 0; i < weapon.Length; i++) {
				weapon[i] = weapons[i].GetComponent<Weapon>();
			}
			generateDamage();
			GameObject parent = this.transform.parent.gameObject;
			Destroy(parent);
		}
	}
}
