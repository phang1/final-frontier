﻿using UnityEngine;
using System.Collections;

public class AddHealth : MonoBehaviour {

	public int health;

	private PlayerHealth playerHealth;

	// Use this for initialization
	void Start () {
		playerHealth = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerHealth>();
	}

	void Update() {
		transform.Rotate(Vector3.forward * 50f * Time.deltaTime);
	}
	
	public void generateHealth() {
		health = Random.Range(5, 11);
		playerHealth.addHealth(health);
	}

	public void OnTriggerEnter(Collider other) {
		if(other.CompareTag("Player")) {
			generateHealth();
			GameObject parent = this.transform.parent.gameObject;
			Destroy(parent);
		}
	}
}
