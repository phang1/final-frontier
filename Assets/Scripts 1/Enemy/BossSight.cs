﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BossSight : MonoBehaviour {
	
	public static Vector3 playerLocation;
	public int damage = 10;
	public EnemyHealth enemyHealth;
	public Vector3 distance;
	public Vector3 minDistance;
	public Vector3 enemyPosition;

	//public AudioClip shoot;
	//public AudioSource source;

	private bool attacking = false;
	private bool basic = false;
	private bool strong = false;
	private bool lunging = false;
	public bool alive = true;
	private NavMeshAgent nav;
	private GameObject player;
	//private PlayerHealth health;
	private Animator anim;
	private WeaponCollider weapon;

	// Use this for initialization
	void Start () {
		player = GameObject.FindGameObjectWithTag ("Player");
		//health = player.gameObject.GetComponent<PlayerHealth> ();
		enemyHealth = GetComponent<EnemyHealth> ();
		anim = GetComponent<Animator> ();
		//source = GetComponent<AudioSource> ();
		nav = GetComponent<NavMeshAgent> ();
		weapon = GetComponent<WeaponCollider>();
	}
	
	// Update is called once per frame
	void Update () {
		alive = enemyHealth.alive;
		playerLocation = player.transform.position;
		distance = playerLocation - transform.position;

		// Set the last global sighting is the players current position.
		if (alive && distance.magnitude > 40f) {
			Move();
			if(Random.Range(0, 1000) < 5 && !attacking) {
				CloseDistance();
			}
		}
		else if (alive && distance.magnitude <= 40f) {
			transform.LookAt(playerLocation);
			if(Random.Range(0, 1000) < 5 && !attacking) {
				BasicAttack();
			}
			else if(Random.Range(0, 1000) > 995 && !attacking) {
				StrongAttack();
			}
		}
		
		if(alive && lunging) {
			transform.position = Vector3.Lerp(transform.position, playerLocation, 5f * Time.deltaTime);
		}

		if(alive && basic || alive && strong) {
			transform.LookAt(playerLocation);
			transform.position = Vector3.Lerp(transform.position, playerLocation, 1f * Time.deltaTime);
		}
	}

	public void Move() {
		playerLocation.y = 0f;
		transform.LookAt (playerLocation);
		nav.destination = playerLocation;
		nav.speed = 10.0f;
	}

	public void CreateDistance() {
		anim.SetTrigger("CreateDistance");
		CancelInvoke();
	}

	public void CloseDistance() {
		attacking = true;
		anim.SetTrigger("GapCloser");
		Invoke("CloseDistanceStrike", 2f);
	}

	public void CloseDistanceStrike() {
		anim.SetTrigger("GapAttack");
		attacking = false;
	}

	public void SpearTurn() {
		anim.SetTrigger("SpearTurn");
		CancelInvoke();
	}

	public void BasicAttack() {
		transform.LookAt(playerLocation);
		anim.SetTrigger("BasicAttack");
		Basic();
	}

	public void StrongAttack() {
		//nav.destination = playerLocation;
		anim.SetTrigger("StrongAttack");
		Strong();
	}

	public void Lunging() {
		weapon.TriggerOn();
		lunging = true;
	}

	public void CancelLunge() {
		weapon.TriggerOff();
		lunging = false;
	}

	public void Strong() {
		attacking = true;
		weapon.TriggerOn();
		strong = true;
	}

	public void CancelStrong() {
		attacking = false;
		weapon.TriggerOff();
		strong = false;
	}

	public void Basic() {
		attacking = true;
		weapon.TriggerOn();
		basic = true;
	}

	public void CancelBasic() {
		attacking = false;
		weapon.TriggerOff();
		basic = false;
	}
}