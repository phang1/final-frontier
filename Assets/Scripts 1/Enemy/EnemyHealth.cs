﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class EnemyHealth : MonoBehaviour {

	public int health = 200;
	public bool alive = true;

	public GameObject healthPickup;
	public GameObject healthDrop;
	public GameObject damageDrop;
	public GameObject stamDrop;

	private NavMeshAgent nav;
	private CapsuleCollider col;
	private Animator anim;

	// Use this for initialization
	void Start () {
		nav = GetComponent<NavMeshAgent>();
		anim = GetComponent<Animator> ();
		col = GetComponent<CapsuleCollider>();
	}
	
	// Update is called once per frame
	void Update () {
		if (health <= 0 && alive) {
			Dead ();
		}
	}
	public void Dead() {
		alive = false;
		anim.SetBool("Die", true);
		col.enabled = false;
		nav.Stop();
		Vector3 offset = transform.position + transform.up * 5f;
		int drop = Random.Range(0, 10);
		switch(drop) {
			case 0:
				break;
			case 1:
				Instantiate(healthPickup, offset, Quaternion.identity);
				break;
			case 2:
				Instantiate(healthPickup, offset, Quaternion.identity);
				break;
			case 3:
				Instantiate(healthPickup, offset, Quaternion.identity);
				break;
			case 4:
				Instantiate(healthDrop, offset, Quaternion.identity);
				break;
			case 5:
				Instantiate(damageDrop, offset, Quaternion.identity);
				break;
			case 6:
				Instantiate(stamDrop, offset, Quaternion.identity);
				break;
			default:
				Debug.Log("Sorry no drops for you :^)");
				break;
		}
		Invoke("Delete", 10f);
	}

	public void Delete() {
		Destroy (gameObject);
	}

	public void TakeDamage(int damage) {
		anim.SetTrigger ("Stagger");
		health -= damage;
	}
}
