﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class EnemySight : MonoBehaviour {
	
	public static Vector3 playerLocation;
	public int damage = 10;
	public EnemyHealth enemyHealth;
	public Vector3 distance;
	public Vector3 minDistance;
	public Vector3 enemyPosition;
	//public AudioClip shoot;
	//public AudioSource source;

	private bool alive = true;
	private NavMeshAgent nav;
	private GameObject player;
	//private PlayerHealth health;
	private Animator anim;
	private BoxCollider weapon;

	// Use this for initialization
	void Start () {
		player = GameObject.FindGameObjectWithTag ("Player");
		//health = player.gameObject.GetComponent<PlayerHealth> ();
		enemyHealth = GetComponent<EnemyHealth> ();
		anim = GetComponent<Animator> ();
		//source = GetComponent<AudioSource> ();
		nav = GetComponent<NavMeshAgent> ();
		weapon = GetComponentInChildren<BoxCollider>();
	}
	
	// Update is called once per frame
	void Update () {
		alive = enemyHealth.alive;
		playerLocation = player.transform.position;
		distance = playerLocation - transform.position;

		// Set the last global sighting is the players current position.
		if (alive && distance.magnitude > 20f) {
			transform.LookAt (playerLocation);
			Invoke("Move", Random.Range (1, 3));
		}
		else if (alive && distance.magnitude <= 20f) {
			Shuffle ();
		}
		if(alive && weapon.enabled) {
			transform.LookAt(playerLocation);
			transform.position = Vector3.Lerp(transform.position, playerLocation, 1f * Time.deltaTime);
		}
	}

	public void Shuffle() {
		anim.SetBool ("Run", false);
		nav.speed = 5.0f;
		anim.SetBool ("Shuffle", true);
		nav.updateRotation = false;
		transform.LookAt(player.transform.position);
		nav.destination = playerLocation;
		Invoke("Attack", Random.Range (2, 4));
	}

	public void Move() {
		anim.SetBool ("Shuffle", false);
		anim.SetBool ("Run", true);
		playerLocation.y = 0f;
		transform.LookAt (playerLocation);
		nav.destination = playerLocation;
		nav.speed = 10.0f;
		anim.SetInteger("Attack", 0);
		CancelInvoke();
	}

	public void Attack() {
		//nav.destination = playerLocation;
		anim.SetBool ("Shuffle", false);
		int attack = Random.Range (0, 4);
		Debug.Log (attack);
		switch (attack) {
		case 0: anim.SetTrigger ("Strong");
			break;
		case 1: anim.SetInteger("Attack", 1);
			anim.SetTrigger("firstAttack");
			break;
		case 2: anim.SetInteger("Attack", 2);
			anim.SetTrigger ("firstAttack");
			break;
		case 3: anim.SetInteger("Attack", 3);
			anim.SetTrigger("firstAttack");
			break;
		}
		CancelInvoke ();
	}
}
