﻿using UnityEngine;
using System.Collections;

public class GunSight : MonoBehaviour {
	public static Vector3 playerLocation;
	public int damage = 10;
	public EnemyHealth enemyHealth;
	public Vector3 distance;
	public GameObject prefab;

	private bool shooting = false;
	private bool alive = true;
	private NavMeshAgent nav;
	private GameObject player;
	private Animator anim;

	// Use this for initialization
	void Start () {
		nav = GetComponent<NavMeshAgent> ();
		player = GameObject.FindGameObjectWithTag ("Player");
		anim = GetComponent<Animator> ();
		enemyHealth = GetComponent<EnemyHealth> ();
	}
	
	// Update is called once per frame
	void Update () {
		alive = enemyHealth.alive;
		playerLocation = player.transform.position;
		distance = playerLocation - transform.position;

		if (alive) {
			if(distance.magnitude < 20f) {
				RunAway();
			}
			else {
				transform.LookAt(playerLocation);
				anim.SetBool("Walk", false);
				Invoke ("Shoot", Random.Range (2, 4));
			}
		}
	}

	public void Shoot() {
		anim.SetTrigger ("Shoot");
		shooting = true;
		Instantiate (prefab, transform.GetChild (3).transform.position, Quaternion.identity);
		CancelInvoke ();
	}

	public void CancelShoot() {
		shooting = false;
	}

	public void RunAway() {
		Vector3 minDistance = player.transform.position - distance.normalized * 40f;
		anim.SetBool("Walk", true);
		nav.speed = 5.0f;
		nav.updateRotation = false;
		transform.LookAt (playerLocation);
		nav.destination = minDistance;
	}
}
