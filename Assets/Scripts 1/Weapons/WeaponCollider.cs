﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class WeaponCollider : MonoBehaviour {

	public BoxCollider weaponCollider;

	void Start() {
		weaponCollider = GetComponentInChildren<BoxCollider> ();
		weaponCollider.enabled = false;
	}
	
	public void TriggerOn() {
		weaponCollider.enabled = true;
	}
	
	public void TriggerOff() {
		weaponCollider.enabled = false;
	}

	public void OnTriggerEnter(Collider other) {
		if(other.CompareTag ("Player")) {
			PlayerHealth health = other.GetComponent<PlayerHealth>();
			health.takeDamage (10);
		}
	}
}
