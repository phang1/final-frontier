﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerWeaponCollider : MonoBehaviour {
	
	public BoxCollider[] weaponCollider;
	public static bool attacking = false;

	private PlayerController player;
	private PlayerStamina stam;
	private GameObject[] weapons;

	void Start() {
		weapons = GameObject.FindGameObjectsWithTag ("PlayerWeapon");
		stam = GetComponent<PlayerStamina> ();
		player = GetComponent<PlayerController>();
		weaponCollider = new BoxCollider[2];

		for (int i = 0; i < weapons.Length; i++) {
			weaponCollider [i] = weapons[i].GetComponent<BoxCollider> ();
			weaponCollider [i].enabled = false;
		}
	}
	
	public void TriggerOn() {
		PlayerController.speed = 10f;
		CancelInvoke();
		for(int i = 0; i < weapons.Length; i++) 
			weaponCollider[i].enabled = true;
		attacking = true;
		stam.staminaSlider.value -= 10f;
		stam.moveUsed = true;
		Invoke("TriggerOff", 1);
	}
	
	public void TriggerOff() {
		PlayerController.speed = 20f;
		for(int i = 0; i < weapons.Length; i++) 
			weaponCollider[i].enabled = false;
		attacking = false;
		stam.moveUsed = false;
		//player.speed = 10f;
		CancelInvoke();
	}
}
