﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Weapon : MonoBehaviour {

	public int damage = 10;
	public PlayerHealth playerHealth;
	public GameObject onHitParticle;
	void Start() {
		playerHealth = GetComponentInParent<PlayerHealth>();
	}

	public void OnTriggerEnter(Collider other) {
		Debug.Log (other.name);
		if(other.CompareTag ("Enemy") || other.CompareTag("Boss")) {
			EnemyHealth health = other.GetComponent<EnemyHealth>();
			health.TakeDamage (damage);
			if(PlayerController.lifeSteal) {
				playerHealth.takeHpPack(5f);
			}
			Debug.Log ("it caught the enemy");
			Instantiate (onHitParticle, this.transform.position, Quaternion.identity);
		}
	}
}
