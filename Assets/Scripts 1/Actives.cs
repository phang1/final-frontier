﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Actives : MonoBehaviour {
	public GameObject[] active = new GameObject[13];
	public bool[] activeOrNot = new bool[12];
	// Use this for initialization
	void Start () {
		for (int i = 0; i <= 11; i++) {
			active [i].SetActive(false);
		}
		for (int i = 0; i <= 11; i++) {
			activeOrNot [i] = false;
		}
	}
	
	// Update is called once per frame
	void OnTriggerStay(Collider other)
	{
		if(other.gameObject.CompareTag("Force Push")){
			if (Input.GetKey (KeyCode.Alpha1)) {
				for (int i = 0; i <= 6; i++) {
					active [i].SetActive (false);
					active [i].GetComponent<Image> ().color = new Color (1f, 1f, 1f, 1f);
					active [12].SetActive (false);
					activeOrNot [i] = false;
				}
				Destroy (other.gameObject);
				active [0].SetActive (true);
				activeOrNot[0] = true;
			}
		}
		if(other.gameObject.CompareTag("Chain Lightning")){
			if (Input.GetKey (KeyCode.Alpha1)) {
				for (int i = 0; i <= 6; i++) {
					active [i].SetActive (false);
					active [i].GetComponent<Image> ().color = new Color (1f, 1f, 1f, 1f);
					active [12].SetActive (false);
					activeOrNot [i] = false;
				}
				Destroy (other.gameObject);
				active [1].SetActive (true);
				activeOrNot [1] = true;
			}
		}
		if(other.gameObject.CompareTag("SoulMass")){
			if (Input.GetKey (KeyCode.Alpha1)) {
				for (int i = 0; i <= 6; i++) {
					active [i].SetActive (false);
					active [i].GetComponent<Image> ().color = new Color (1f, 1f, 1f, 1f);
					active [12].SetActive (false);
					activeOrNot [i] = false;
				}
				Destroy (other.gameObject);
				active [2].SetActive (true);
				activeOrNot [2] = true;
			}
		}
		/*if(other.gameObject.CompareTag("")){
			if (Input.GetKey (KeyCode.Alpha1)) {
				for (int i = 0; i <= 6; i++) {
					active [i].SetActive (false);
					active [i].GetComponent<Image> ().color = new Color (1f, 1f, 1f, 1f);
					active [12].SetActive (false);
					activeOrNot [i] = false;
				}
				Destroy (other.gameObject);
				active [3].SetActive (true);
				activeOrNot [3] = true;
			}
		}
		if(other.gameObject.CompareTag("")){
			if (Input.GetKey (KeyCode.Alpha1)) {
				for (int i = 0; i <= 6; i++) {
					active [i].SetActive (false);
					active [i].GetComponent<Image> ().color = new Color (1f, 1f, 1f, 1f);
					active [12].SetActive (false);
					activeOrNot [i] = false;
				}
				Destroy (other.gameObject);
				active [4].SetActive (true);
				activeOrNot [4] = true;
			}
		}
		if(other.gameObject.CompareTag("")){
			if (Input.GetKey (KeyCode.Alpha1)) {
				for (int i = 0; i <= 6; i++) {
					active [i].SetActive (false);
					active [i].GetComponent<Image> ().color = new Color (1f, 1f, 1f, 1f);
					active [12].SetActive (false);
					activeOrNot [i] = false;
				}
				Destroy (other.gameObject);
				active [5].SetActive (true);
				activeOrNot [5] = true;
			}
		}*/

		if(other.gameObject.CompareTag("Force Push")){
			if (Input.GetKey (KeyCode.Alpha2)) {
				for (int i = 6; i <= 11; i++) {
					active [i].SetActive (false);
					active [i].GetComponent<Image> ().color = new Color (1f, 1f, 1f, 1f);
					active [13].SetActive (false);
					activeOrNot [i] = false;
				}
				Destroy (other.gameObject);
				active [6].SetActive (true);
				activeOrNot [6] = true;
			}
		}
		if(other.gameObject.CompareTag("Chain Lightning")){
			if (Input.GetKey (KeyCode.Alpha2)) {
				for (int i = 6; i <= 11; i++) {
					active [i].SetActive (false);
					active [i].GetComponent<Image> ().color = new Color (1f, 1f, 1f, 1f);
					active [13].SetActive (false);
					activeOrNot [i] = false;
				}
				Destroy (other.gameObject);
				active [7].SetActive (true);
				activeOrNot [7] = true;
			}
		}
		if(other.gameObject.CompareTag("SoulMass")){
			if (Input.GetKey (KeyCode.Alpha2)) {
				for (int i = 6; i <= 11; i++) {
					active [i].SetActive (false);
					active [i].GetComponent<Image> ().color = new Color (1f, 1f, 1f, 1f);
					active [13].SetActive (false);
					activeOrNot [i] = false;
				}
				Destroy (other.gameObject);
				active [8].SetActive (true);
				activeOrNot [8] = true;
			}
		}
		/*if(other.gameObject.CompareTag("")){
			if (Input.GetKey (KeyCode.Alpha2)) {
				for (int i = 6; i <= 11; i++) {
					active [i].SetActive (false);
					active [i].GetComponent<Image> ().color = new Color (1f, 1f, 1f, 1f);
					active [13].SetActive (false);
					activeOrNot [i] = false;
				}
				Destroy (other.gameObject);
				active [9].SetActive (true);
				activeOrNot [9] = true;
			}
		}
		if(other.gameObject.CompareTag("")){
			if (Input.GetKey (KeyCode.Alpha2)) {
				for (int i = 6; i <= 11; i++) {
					active [i].SetActive (false);
					active [i].GetComponent<Image> ().color = new Color (1f, 1f, 1f, 1f);
					active [13].SetActive (false);
					activeOrNot [i] = false;
				}
				Destroy (other.gameObject);
				active [10].SetActive (true);
				activeOrNot [10] = true;
			}
		}
		if(other.gameObject.CompareTag("")){
			if (Input.GetKey (KeyCode.Alpha2)) {
				for (int i = 6; i <= 11; i++) {
					active [i].SetActive (false);
					active [i].GetComponent<Image> ().color = new Color (1f, 1f, 1f, 1f);
					active [13].SetActive (false);
					activeOrNot [i] = false;
				}
				Destroy (other.gameObject);
				active [11].SetActive (true);
				activeOrNot [11] = true;
			}
		}*/
	}
	public void cooldown(){
		for (int i = 0; i <= 6; i++) {
			active [i].GetComponent<Image>().color = new Color (167f/255f, 167f/255f, 167f/255f, 141f/255f);
			Invoke ("offCD", 8);
		}
	}
	public void cooldown2(){
		for (int i = 6; i <= 11; i++) {
			active [i].GetComponent<Image>().color = new Color (167f/255f, 167f/255f, 167f/255f, 141f/255f);
			Invoke ("offCD2", 8);
		}
	}
	private void offCD(){
		for (int i = 0; i <= 6; i++) {
			active [i].GetComponent<Image>().color = new Color (1f,1f,1f,1f);
		}
	}
	private void offCD2(){
		for (int i = 6; i <= 11; i++) {
			active [i].GetComponent<Image>().color = new Color (1f,1f,1f,1f);
		}
	}
}
