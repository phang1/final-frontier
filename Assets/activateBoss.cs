﻿using UnityEngine;
using System.Collections;

public class activateBoss : MonoBehaviour {

	public BossSight boss;

	// Use this for initialization
	void Start () {
		boss.enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider other){
		if (other.tag == "Player") {
			boss.enabled = true;
		}
	}
}
